#!/usr/bin/env bash 

export SPAKL_CERTS_DIR=/skutils/certs
export SPAKL_FILES_DIR=/skutils/files
export SPAKL_SCRIPTS_DIR=/skutils/scripts
export SPAKL_BIN_DIR=/skutils/bin


chmod -R +x /skutils/bin
chmod -R +x /skutils/scripts