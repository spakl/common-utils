#!/usr/bin/env bash

function spakl_tf_gitlab_init(){
    #!/bin/bash
    DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)

    GITLAB_BOT_USER=${GITLAB_USER}
    GITLAB_BOT_TOKEN=${GITLAB_TOKEN}

    if [[ -z "$PROJECT_ID" ]]; then
    echo "ERROR: PROJECT_ID is required"
    exit 1;    
    fi

    if [[ -z "$STATE_NAME" ]]; then
    echo "ERROR: STATE_NAME is required"
    exit 1;
    fi

    if [[ -z "$GITLAB_BOT_USER" ]]; then
    echo "ERROR: GITLAB_BOT_USER is required"
    exit 1;
    fi


    if [[ -z "$GITLAB_BOT_TOKEN" ]]; then
    echo "ERROR: GITLAB_BOT_TOKEN is required"
    exit 1;
    fi


    terraform init $@ \
      -backend-config="address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}" \
      -backend-config="lock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}/lock" \
      -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}/lock" \
      -backend-config="username=$GITLAB_BOT_USER" \
      -backend-config="password=$GITLAB_BOT_TOKEN" \
      -backend-config="lock_method=POST" \
      -backend-config="unlock_method=DELETE" \
      -backend-config="retry_wait_min=5"
}