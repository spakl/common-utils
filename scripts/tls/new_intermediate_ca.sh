#!/usr/bin/env bash
# SPAKL_UTILS_DIR=/spakl-utils
if [[ -z "$SPAKL_CERTS_DIR" ]]; then 
    echo "NO SPAKL_UTILS_DIR, DEFAULTING TO LOCAL UTILS"
    SPAKL_CERTS_DIR=$PWD/certs
    echo $DIR
fi 


# Default values
SPAKL_CERTS_DIR="$PWD/certs" # Default directory for certificates
ROOT_CA_NAME="spakl-ca" # Default root CA name
INTERMEDIATE_CA_NAME="intermediate" # Default intermediate CA name
DAYS_VALID=3650 # Default validity of the certificate
SUBJECT_CN="intermediate.spakl.io" # Default Common Name for the certificate

# Help function
show_help() {
    echo "Usage: $0 [options]"
    echo ""
    echo "Options:"
    echo "  -c, --ca-name <name>              Specify the root CA name (default: 'spakl-ca')"
    echo "  -i, --intermediate-ca-name <name> Specify the intermediate CA name (default: 'intermediate')"
    echo "  -d, --days-valid <days>           Specify the number of days the certificate is valid (default: 3650)"
    echo "  -n, --subject-cn <common name>    Specify the subject's common name (default: 'intermediate.spakl.io')"
    echo "  -h, --help                        Display this help and exit"
}

# Parse command-line options using getopt
TEMP=$(getopt -o 'c:i:d:n:h' --long 'ca-name:,intermediate-ca-name:,days-valid:,subject-cn:,help' -- "$@")
if [ $? != 0 ]; then echo "Failed parsing options." >&2; exit 1; fi
eval set -- "$TEMP"

# Process each option
while true; do
    case "$1" in
        -c|--ca-name)
            ROOT_CA_NAME="$2"
            shift 2
            ;;
        -i|--intermediate-ca-name)
            INTERMEDIATE_CA_NAME="$2"
            shift 2
            ;;
        -d|--days-valid)
            DAYS_VALID="$2"
            shift 2
            ;;
        -n|--subject-cn)
            SUBJECT_CN="$2"
            shift 2
            ;;
        -h|--help)
            show_help
            exit 0
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Creating the intermediate certificate
mkdir -p "$SPAKL_CERTS_DIR/$INTERMEDIATE_CA_NAME"

# Generate the intermediate CA private key
openssl genrsa -out "$SPAKL_CERTS_DIR/$INTERMEDIATE_CA_NAME/${INTERMEDIATE_CA_NAME}-key.pem" 4096

# Create the intermediate CA certificate signing request (CSR)
openssl req -new -sha256 \
    -key "$SPAKL_CERTS_DIR/$INTERMEDIATE_CA_NAME/${INTERMEDIATE_CA_NAME}-key.pem" \
    -subj "/CN=${SUBJECT_CN}" \
    -out "$SPAKL_CERTS_DIR/$INTERMEDIATE_CA_NAME/${INTERMEDIATE_CA_NAME}.csr"

# Sign the intermediate CA CSR with the root CA to create the intermediate CA certificate
openssl x509 -req -days "${DAYS_VALID}" -sha256 \
    -in "$SPAKL_CERTS_DIR/$INTERMEDIATE_CA_NAME/${INTERMEDIATE_CA_NAME}.csr" \
    -CA "$SPAKL_CERTS_DIR/${ROOT_CA_NAME}.pem" \
    -CAkey "$SPAKL_CERTS_DIR/${ROOT_CA_NAME}-key.pem" \
    -CAcreateserial \
    -out "$SPAKL_CERTS_DIR/$INTERMEDIATE_CA_NAME/${INTERMEDIATE_CA_NAME}.pem"

rm $SPAKL_CERTS_DIR/$INTERMEDIATE_CA_NAME/${INTERMEDIATE_CA_NAME}.csr

echo "Intermediate CA certificate created at: $SPAKL_CERTS_DIR/$INTERMEDIATE_CA_NAME/${INTERMEDIATE_CA_NAME}.pem"
