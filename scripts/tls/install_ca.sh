#!/usr/bin/env bash

CA_PEM_NAME="spakl-ca" # Default CA name
OS_TYPE="debian" # OS type will be determined or can be set manually

show_help() {
    echo "Usage: $0 [options]"
    echo ""
    echo "Options:"
    echo "  -c, --ca-name <name>    Specify the CA name (default: 'spakl-ca')"
    echo "  -o, --os-type <type>    Specify the operating system type ('debian', 'fedora') (default: 'debian')"
    echo "  -h, --help              Display this help and exit"
    echo ""
    echo "If the operating system type is not specified, the script attempts to determine it automatically."
}

# Parse command-line options using getopt
TEMP=$(getopt -o 'c:o:h' --long 'ca-name:,os-type:help' -- "$@")
if [ $? != 0 ]; then echo "Failed parsing options." >&2; exit 1; fi
eval set -- "$TEMP"

# Process each option
while true; do
    case "$1" in
        -h|--help)
            show_help
            exit 0
            ;;
        -c|--ca-name)
            CA_PEM_NAME="$2"
            shift 2
            ;;
        -o|--os-type)
            OS_TYPE="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Check for SPAKL_CERTS_DIR environment variable and set it if not present
if [[ -z "$SPAKL_CERTS_DIR" ]]; then 
    echo "NO SPAKL_CERTS_DIR, DEFAULTING TO LOCAL UTILS"
    SPAKL_CERTS_DIR=$PWD/certs
    echo "$SPAKL_CERTS_DIR"
fi 


# Update CA certificates based on the OS_TYPE
case $OS_TYPE in
    "debian")
        cp "$SPAKL_CERTS_DIR/${CA_PEM_NAME}/${CA_PEM_NAME}.pem" "/usr/local/share/ca-certificates/${CA_PEM_NAME}.crt"
        update-ca-certificates
        ls -l /etc/ssl/certs/ | grep ${CA_PEM_NAME}.crt
        ;;
    "fedora")
        cp "$SPAKL_CERTS_DIR/${CA_PEM_NAME}/${CA_PEM_NAME}.pem" "/usr/share/pki/ca-trust-source/anchors/${CA_PEM_NAME}.crt"
        update-ca-trust
        ;;
    *)
        echo "Unsupported OS type or OS type not specified."
        ;;
esac
