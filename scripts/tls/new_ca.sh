#!/usr/bin/env bash
if [[ -z "$SPAKL_CERTS_DIR" ]]; then 
    echo "NO SPAKL_UTILS_DIR, DEFAULTING TO LOCAL UTILS"
    SPAKL_CERTS_DIR=$PWD/certs
    echo $DIR
fi 

show_help() {
    echo "Usage: $0 [options]"
    echo ""
    echo "Options:"
    echo "  -a, --ca-name <name>    Specify the CA name (default: 'spakl-ca')"
    echo "  -h, --help              Display this help and exit"
}

# Default value for CA name
CA_PEM_NAME="spakl-ca" # Default CA name

# Parse command-line options using getopt
TEMP=$(getopt -o 'a:h' --long 'ca-name:help' -- "$@")
if [ $? != 0 ]; then echo "Failed parsing options." >&2; exit 1; fi
eval set -- "$TEMP"

# Process each option
while true; do
    case "$1" in
        -h|--help)
            show_help
            exit 0
            ;;
        -a|--ca-name)
            CA_PEM_NAME="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

mkdir -p $SPAKL_CERTS_DIR/${CA_PEM_NAME}

openssl genrsa -aes256 -out $SPAKL_CERTS_DIR/${CA_PEM_NAME}/${CA_PEM_NAME}-key.pem 4096

openssl req -new -x509 -sha256 -days 3650 -key $SPAKL_CERTS_DIR/${CA_PEM_NAME}/${CA_PEM_NAME}-key.pem -out $SPAKL_CERTS_DIR/${CA_PEM_NAME}/${CA_PEM_NAME}.pem

# View Results
openssl x509 -in $SPAKL_CERTS_DIR/${CA_PEM_NAME}/${CA_PEM_NAME}.pem -text
