#!/usr/bin/env bash
if [[ -z "$SPAKL_CERTS_DIR" ]]; then 
    echo "NO SPAKL_UTILS_DIR, DEFAULTING TO LOCAL UTILS"
    SPAKL_CERTS_DIR=$PWD/certs
    echo $DIR
fi 


# Default values for options
CA_PEM_NAME="spakl-ca" # Default CA name
CERT_PEM_NAME="cert" # Default certificate name
DAYS_VALID=3650 # Default validity of the certificate
SUBJECT_CN="spakl.io" # Default Common Name for the certificate
SUBJECT_ALT_NAME="DNS:*.null.spakl,IP:10.10.4.240" # Default Subject Alternative Names
EXTENDED_KEY_USAGE="serverAuth" # Default Extended Key Usage


show_help() {
    echo "Usage: $0 [options]"
    echo ""
    echo "Options:"
    echo "  -a, --ca-name <name>              Specify the CA name (default: 'spakl-ca')"
    echo "  -b, --cert-name <name>            Specify the certificate name (default: 'cert')"
    echo "  -c, --days-valid <days>           Specify the number of days the certificate is valid (default: 3650)"
    echo "  -d, --subject-cn <common name>    Specify the subject's common name (default: 'spakl.io')"
    echo "  -e, --subject-alt-name <SAN>      Specify the subject alternative names (default: 'DNS:*.null.spakl,IP:10.10.4.240')"
    echo "      --extended-key-usage <usage>  Specify the extended key usage (default: 'serverAuth')"
    echo "  -h, --help                        Display this help and exit"
}
# Parse command-line options using getopt
TEMP=$(getopt -o 'a:b:c:d:e:h' --long 'ca-name:,cert-name:,days-valid:,subject-cn:,subject-alt-name:,extended-key-usage:help' -- "$@")
if [ $? != 0 ]; then echo "Failed parsing options." >&2; exit 1; fi
eval set -- "$TEMP"

# Process each option
while true; do
    case "$1" in
        -h|--help)
            show_help
            exit 0
            ;;
        -a|--ca-name)
            CA_PEM_NAME="$2"
            shift 2
            ;;
        -b|--cert-name)
            CERT_PEM_NAME="$2"
            shift 2
            ;;
        -c|--days-valid)
            DAYS_VALID="$2"
            shift 2
            ;;
        -d|--subject-cn)
            SUBJECT_CN="$2"
            shift 2
            ;;
        -e|--subject-alt-name)
            SUBJECT_ALT_NAME="$2"
            shift 2
            ;;
        --extended-key-usage)
            EXTENDED_KEY_USAGE="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

mkdir -p ${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}

# Generating the key and CSR
openssl genrsa -out "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}-key.pem" 4096

openssl req -new -sha256 \
    -subj "/CN=${SUBJECT_CN}" \
    -key "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}-key.pem" \
    -out "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}.csr"

# Create the extension file
echo "subjectAltName=${SUBJECT_ALT_NAME}" > "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}-extfile.cnf"
echo "extendedKeyUsage = ${EXTENDED_KEY_USAGE}" >> "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}-extfile.cnf"

# Sign the certificate
openssl x509 -req -sha256 -days "${DAYS_VALID}" \
    -in "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}.csr" \
    -CA "${SPAKL_CERTS_DIR}/${CA_PEM_NAME}/${CA_PEM_NAME}.pem" \
    -CAkey "${SPAKL_CERTS_DIR}/${CA_PEM_NAME}/${CA_PEM_NAME}-key.pem" \
    -CAcreateserial \
    -out "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}.pem" \
    -extfile "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}-extfile.cnf"

# Cleanup
rm "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}-extfile.cnf"
rm "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}.csr"

# Verify the certificate
openssl verify -CAfile "${SPAKL_CERTS_DIR}/${CA_PEM_NAME}/${CA_PEM_NAME}.pem" -verbose "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}.pem"

# Create the fullchain PEM
cat "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}.pem" > "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}-fullchain.pem"
cat "${SPAKL_CERTS_DIR}/${CA_PEM_NAME}/${CA_PEM_NAME}.pem" >> "${SPAKL_CERTS_DIR}/${CERT_PEM_NAME}/${CERT_PEM_NAME}-fullchain.pem"





# # CERT KEY
# openssl genrsa \
#     -out $SPAKL_CERTS_DIR/${CERT_PEM_NAME}-key.pem \
#     4096


# # CERT SIGNING REQUEST
# openssl req -new -sha256 \
#     -subj "/CN=spakl.io" \
#     -key $SPAKL_CERTS_DIR/${CERT_PEM_NAME}-key.pem \
#     -out $SPAKL_CERTS_DIR/${CERT_PEM_NAME}.csr


# # echo "subjectAltName=DNS:*.svc.spakl,IP:10.10.1.1" >> $SPAKL_CERTS_DIR/extfile.cnf
# echo "subjectAltName=DNS:*.svc.spakl" >> $SPAKL_CERTS_DIR/${CERT_PEM_NAME}-extfile.cnf

# echo extendedKeyUsage = serverAuth >> $SPAKL_CERTS_DIR/${CERT_PEM_NAME}-extfile.cnf

# openssl x509 -req -sha256 -days 3650 \
#     -in $SPAKL_CERTS_DIR/${CERT_PEM_NAME}.csr \
#     -CA $SPAKL_CERTS_DIR/${CA_PEM_NAME}.pem \
#     -CAkey $SPAKL_CERTS_DIR/${CA_PEM_NAME}-key.pem \
#     -out $SPAKL_CERTS_DIR/${CERT_PEM_NAME}.pem \
#     -extfile $SPAKL_CERTS_DIR/${CERT_PEM_NAME}-extfile.cnf \
#     -CAcreateserial

# rm $SPAKL_CERTS_DIR/${CERT_PEM_NAME}-extfile.cnf
# rm $SPAKL_CERTS_DIR/${CERT_PEM_NAME}.csr

# openssl verify -CAfile $SPAKL_CERTS_DIR/${CA_PEM_NAME}.pem -verbose $SPAKL_CERTS_DIR/${CERT_PEM_NAME}.pem
    
# cat $SPAKL_CERTS_DIR/${CERT_PEM_NAME}.pem > $SPAKL_CERTS_DIR/${CERT_PEM_NAME}-fullchain.pem
# cat $SPAKL_CERTS_DIR/$CA_PEM_NAME.pem >> $SPAKL_CERTS_DIR/${CERT_PEM_NAME}-fullchain.pem
    