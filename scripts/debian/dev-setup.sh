#!/usr/bin/env bash

set -e 

terraform_version="1.7.4"
vault_version="1.15.5"
go_version="1.22.0"


sudo wget https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
-O /usr/share/keyrings/vscodium-archive-keyring.asc

echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.asc ] https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list

sudo apt update

sudo apt-get install -y \
	jq \
	libc-dev \
	codium \
	codium-insiders \
	git \
	docker.io \
	net-tools \
	neofetch \
	ca-certificates \
	nmap \
	curl \
	zip 


if command -v terraform &> /dev/null; then
    echo "Terraform exists"
else
curl -LO https://releases.hashicorp.com/terraform/${terraform_version}/terraform_${terraform_version}_linux_amd64.zip \
	&& unzip ./terraform_${terraform_version}_linux_amd64.zip -d /tmp \
	&& chmod +x /tmp/terraform \
	&& sudo mv /tmp/terraform /usr/local/bin/ \
	&& rm terraform_${terraform_version}_linux_amd64.zip \
	&& terraform --version
fi

if command -v vault &> /dev/null; then
    echo "Vault exists"
else
curl -LO https://releases.hashicorp.com/vault/${vault_version}/vault_${vault_version}_linux_amd64.zip \
	&& unzip ./vault_${vault_version}_linux_amd64.zip -d /tmp \
	&& chmod +x /tmp/vault \
	&& sudo mv /tmp/vault /usr/local/bin/ \
	&& rm vault_${vault_version}_linux_amd64.zip \
	&& vault --version
fi

if command -v kubectl &> /dev/null; then
    echo "kubectl exists"
else
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256"
echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
kubectl version --client
rm kubectl
rm kubectl.sha256
fi

if command -v helm &> /dev/null; then
    echo "helm exists"
else
curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
fi

if command -v nvm &> /dev/null; then
    echo "nvm exists"
else
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh | bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

fi

if command -v node &> /dev/null; then
    echo "node exists"
else
nvm install node
npm install -g npm@latest
fi


if command -v ng &> /dev/null; then
    echo "Angular ng exists"
else
npm install -g @angular/cli
fi

if command -v tofu &> /dev/null; then
    echo "tofu exists"
else
# Download the installer script:
curl --proto '=https' --tlsv1.2 -fsSL https://get.opentofu.org/install-opentofu.sh -o install-opentofu.sh
# Alternatively: wget --secure-protocol=TLSv1_2 --https-only https://get.opentofu.org/install-opentofu.sh -O install-opentofu.sh
# Grand execution permissions:
chmod +x install-opentofu.sh
# Please inspect the downloaded script at this point.
# Run the installer:
./install-opentofu.sh --install-method standalone
# Remove the installer:
rm install-opentofu.sh
fi

if command -v snyk &> /dev/null; then
    echo "snyk exists"
else
curl --compressed https://static.snyk.io/cli/latest/snyk-linux -o snyk
chmod +x ./snyk
mv ./snyk /usr/local/bin/
fi

if command -v go &> /dev/null; then
    echo "go exists"
else
curl -LO https://go.dev/dl/go${go_version}.linux-amd64.tar.gz 
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go${go_version}.linux-amd64.tar.gz
rm go${go_version}.linux-amd64.tar.gz
echo "export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin" >> ~/.profile
echo 'export GOPATH=$HOME/go/bin' >> ~/.profile
fi



if command -v dive &> /dev/null; then
    echo "dive exists"
else
go install github.com/wagoodman/dive@latest
fi


