VERSION=v1.0.0
build:
	docker build --no-cache -t registry.gitlab.com/spakl/common-utils:${VERSION} . 

push:
	docker push registry.gitlab.com/spakl/common-utils:${VERSION}

encrypt:
	ansible-vault encrypt cert.vault.yml

decrypt:
	ansible-vault decrypt cert.vault.yml